# 1. Написать роль для установки Prometheus
        Done.
        https://gitlab.com/ansible-roles28/ansible-role-prometheus

# 2. Написать роль для установки node-exporter и настроить сбор метрик:
        https://gitlab.com/ansible-roles28/ansible-role-node_exporter
        1. Работоспособность контейнеров.
        Done.
        cAdvisor
        2. Работа Nginx.
        nginx_exporter
        3. Работа приложения.
        https://gitlab.com/ansible-roles28/ansible-role-blackbox_exporter
# 3. Написать роль для развертывания Grafana
        Done.
        https://gitlab.com/ansible-roles28/ansible-role-grafana
# 4. Визуализировать метрики с помощью Grafana
        Done.
# 5. Покрыть роли тестами molecule и автоматизировать тесты через ci для репозитория ролей
        Done.
# 6. Добавить несколько видов алертов - почта, telegramm, email и тд
        https://gitlab.com/ansible-roles28/ansible-role-alertmanager
